package com.atlassian.botocss;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class Classpath {
    static String getResource(String resourcePath) {
        try (InputStream resource = Classpath.class.getResourceAsStream(resourcePath)) {
            if (resource == null) return null;
            return IOUtils.toString(resource, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException("Could not load resource [" + resourcePath + "]: " + e.getMessage(), e);
        }
    }
}
